/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.belajar.marketplace.imageservice.controller;

import java.net.URL;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author opaw
 */
@RestController
public class ImageProductController {
    
    @GetMapping("/api/product/{id}")
    @ResponseBody
    public ResponseEntity<Resource> serveCategoyThumbs(@PathVariable String id) {
        try {
            URL url = new URL("https://sc01.alicdn.com/kf/UTB8CtXfXrPJXKJkSafSq6yqUXXaT/Fresh-Frozen-Raspberry-For-Sell.jpg");
            Resource file = new UrlResource(url);
            return ResponseEntity
                    .ok()
                    .contentType(MediaType.IMAGE_JPEG)
                    .header(HttpHeaders.CONTENT_DISPOSITION, "filename=\"" + file.getFilename() + "\"")
                    .body(file);
        } catch (Exception ex) {
            System.out.println(ex);
            return null;
        }
    }
    
}
